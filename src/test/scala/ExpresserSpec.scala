import org.scalatest._

trait ExpresserSpec  extends Matchers { this: FlatSpec =>
  def expresser(expresser: Expresser) = {

    it should "find an expression for target = 42 and bag = [2,3,5,6]" in {
      val result = expresser.express(42, Seq(2,3,5,6))
      result shouldBe defined
      result.get.value shouldBe 42
    }

    it should "find the trivial expression if target is in the bag" in {
      val result = expresser.express(42, Seq(2,3,5,6,42))
      result shouldBe defined
      result.get shouldBe IntLiteral(42)
    }

    it should "find the easier expression 21 * 2 when the target = 42" in {
      val result = expresser.express(42, Seq(3,4,5,6,21,2))
      result shouldBe defined
      result.get shouldBe BinaryExpression(IntLiteral(21), IntLiteral(2), Times)
    }

    it should "find an expression for target = (251+252)*(254+255) and bag = [251,252,254,255]" in {
      val result = expresser.express((251+252)*(254+255), Seq(251,252,254,255))
        result shouldBe defined
        result.get.value shouldBe (251+252)*(254+255)
    }
  }
}
