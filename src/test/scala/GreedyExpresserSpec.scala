import org.scalatest.{FlatSpec, Matchers}

class GreedyExpresserSpec extends FlatSpec with ExpresserSpec with Matchers {
  "A GreedyExpresser" should behave like(expresser(GreedyExpresser))
}
