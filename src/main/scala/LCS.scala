object LCS extends App {
  // I realise this is not tail recursive but the time complexity is so bad that
  // a stack overflow is not really is a concern practically :)
  def lcs(input1: Seq[Char], input2: Seq[Char]): String = {
      if(input1.size * input2.size == 0)
        ""
      else if(input1.head == input2.head) {
        input1.head + lcs(input1.tail, input2.tail)
      } else {
        val possibility1 = lcs(input1, input2.tail)
        val possibility2 = lcs(input1.tail, input2)
        if(possibility1.size > possibility2.size)
          possibility1
        else
          possibility2
      }
  }

  println(lcs("AABACDA", "DACBBCAD"))
}
