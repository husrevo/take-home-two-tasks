object Main extends App {
  if(args.size < 2) {
    println("usage: java -jar file.jar TARGET INT1 INT2 INT3")
    println("example: java -jar file.jar 42 2 3 5 6")
  } else {
    val target :: bag = args.map(_.toInt).toList
    val result = GreedyExpresser.express(target, bag)
    println(result)
  }
}
