sealed trait BinaryOperator {
  def apply(left: Int, right: Int) : Int
  def unapply(result : Int, right: Int): Option[Int]
  def str : String
  override def toString: String = str
}
object Plus extends BinaryOperator {
  override def apply(left: Int, right: Int): Int = left + right
  override def unapply(result: Int, right: Int): Option[Int] = if(result > right) Some(result - right) else None
  override def str: String = "+"
}

object Minus extends BinaryOperator {
  override def apply(left: Int, right: Int): Int = left - right
  override def unapply(result: Int, right: Int): Option[Int] = Some(result + right)
  override def str: String = "-"
}

object Times extends BinaryOperator {
  override def apply(left: Int, right: Int): Int = left * right
  override def unapply(result: Int, right: Int): Option[Int] = if(result % right == 0 && right != 1) Some(result/right) else None
  override def str: String = "*"
}

sealed trait Expression {
  def strRepr : String
  override def toString: String = strRepr
  def value : Int
}
case class IntLiteral(value: Int) extends Expression {
  override def strRepr: String = value.toString
}
case class BinaryExpression(left: Expression, right: Expression, operator: BinaryOperator) extends Expression {
  override def strRepr: String = s"($left $operator $right)"
  override def value: Int = operator(left.value, right.value)
}

trait Expresser {
  def express(target: Int, using: Seq[Int]) : Option[Expression]
}

object GreedyExpresser extends Expresser {
  val operators = Seq(Times, Plus, Minus)
  override def express(target: Int, using: Seq[Int]): Option[Expression] = {
    if(using.contains(target)) Some(IntLiteral(target)) else {
      val allPossibilities = for{
        op <- operators
        number <- using.distinct
      } yield (op, number)

      allPossibilities.toStream.flatMap { case (op, number) =>
        op.unapply(target, number).toList.flatMap{ nextTarget =>
          val rest = using.diff(Seq(number))
          express(nextTarget, rest).map(leftExp => BinaryExpression(leftExp, IntLiteral(number), op))
        }
      }.headOption
    }
  }
}
